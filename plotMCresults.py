import numpy as np
import matplotlib.pyplot as plt

probabilities = []
times = []
iterations = 0
with open("MCResults/haloSat-iridium_times2FirstContact.txt", "r") as file:
    for line in file:
        iterations += 1
        time = float(line)
        times.append(time)
print("file read")
for i in range(1,6001):
    counter = 0
    for t in times:
        if t < i:
            counter += 1
    probability = counter/float(iterations)
    probabilities.append(probability)

plt.plot(range(1,6001), probabilities)
plt.xlabel("time (s)")
plt.ylabel("cumulative probability")
plt.show()

#plt.hist(times, bins=200)
#plt.show()

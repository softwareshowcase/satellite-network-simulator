import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from astropy import units
from satTools_TLE import satellite, network, cubeSAT

class AnimatedScatter(object):
    def __init__(self):
        self.wholeNetwork = network()
        # Setup the figure and axes...
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111,projection="aitoff")
        # self.ax = self.fig.add_subplot(111)        
        # Then setup FuncAnimation.
        self.ani = animation.FuncAnimation(self.fig, self.update, interval=1, init_func=self.setup_plot, blit=False)

    def setup_plot(self):
        """Initial drawing of the scatter plot."""
        pairs, clrs, sizes = self.grabCurrentPositions()
        self.scat = self.ax.scatter(pairs[:,0],pairs[:,1],c=clrs, s=sizes)
        # For FuncAnimation's sake, we need to return the artist we'll be using
        # Note that it expects a sequence of artists, thus the trailing comma.
        return self.scat,

    def grabCurrentPositions(self):
        longs, lats, clrs, sizes = [], [], [], []
        colorPalette = [1, 2, 3, 4, 5, 6]
        for sat in self.wholeNetwork.satellites:
            longs.append(sat.longitude)
            lats.append(sat.latitude)
            clrs.append(colorPalette[sat.plane-1])
            if sat.UTVisible:
                sizes.append(200)
            else:
                sizes.append(10)
        longs.append(self.wholeNetwork.cubeSAT.longitude)
        lats.append(self.wholeNetwork.cubeSAT.latitude)
        clrs.append(0)
        sizes.append(100)
        coordPairs = np.c_[longs,lats]
        return coordPairs, clrs, sizes
    
    def update(self, i):
        """Update the scatter plot."""
        self.wholeNetwork.advanceTime()
        pairs, clrs, sizes = self.grabCurrentPositions()
        # Set x and y data...
        self.scat.set_offsets(pairs)
        # Set sizes...
        self.scat.set_sizes(np.asarray(sizes))
        # Set colors..
        self.scat.set_array(np.asarray(clrs))
        # We need to return the updated artist for FuncAnimation to draw..
        # Note that it expects a sequence of artists, thus the trailing comma.
        return self.scat,

if __name__ == '__main__':
    a = AnimatedScatter()
    plt.show()

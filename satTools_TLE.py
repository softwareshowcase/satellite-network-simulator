import numpy as np
import matplotlib.pyplot as plt
from astropy import units as u
# import multiprocessing as mp
import time
from skyfield.api import load
from skyfield.api import EarthSatellite
from datetime import datetime, timedelta
import os

class TLE:
    def __init__(self, line1, line2):
        self.line1 = line1
        self.line2 = line2
        self.year = int(line1[18:20])
        if self.year < 57:
            self.year += 2000
        else:
            # TLE start year is 1957
            self.year += 1900
        decimalDays = float(line1[20:32])
        date = datetime(self.year, 1, 1) + timedelta(decimalDays-1)
        self.month = date.month
        self.day = date.day
        self.hour = date.hour
        self.minute = date.minute
        self.second = date.second
        self.microsecond = date.microsecond

class TLEsOverYear:
    def __init__(self, TLEsFile, isNode):
        self.TLEs = []
        for month in range(12):
            self.TLEs.append([])
            for day in range(31):
                self.TLEs[month].append([])
        with open(TLEsFile, 'r') as file:
            if isNode:
                self.plane = int(TLEsFile[14])
            linePair = []
            for line in file:
                if len(linePair) < 2:
                    linePair.append(line)
                else:
                    tle = TLE(linePair[0], linePair[1])
                    monthIndex = tle.month - 1
                    dayIndex = tle.day - 1
                    self.TLEs[monthIndex][dayIndex].append(tle)
                    linePair = []
                    linePair.append(line)

class satellite:
    def __init__(self, num, name, TLEs, delta):
        self.satNum = num
        self.name = name
        self.TLEContainer = TLEsOverYear(TLEs, True)
        self.plane = self.TLEContainer.plane
        breakOuter = False
        # Grab TLE from day 1 or the first one of the year
        try:
            day1TLE = self.TLEContainer.TLEs[0][0][0]
            print("Got the day1 TLE from the TLE Container")
        except:
            for month in range(12):
                if breakOuter:
                    break
                for day in range(31):
                    dayTLEArray = self.TLEContainer.TLEs[month][day]
                    if len(dayTLEArray) > 0:
                        day1TLE = dayTLEArray[0]
                        breakOuter = True
                        print("Grabbed the first available TLE:",day1TLE.month,day1TLE.day)
                        break
        self.currentTLE = day1TLE
        self.ts = load.timescale(builtin=True)
        self.date = datetime(day1TLE.year, 1, 1) # year, month, day
        self.t = self.ts.utc(day1TLE.year, 1, 1)
        self.timeStep = 1.0/(60*60*24)*delta # second * delta expressed as fraction of day

        self.eSatellite = EarthSatellite(day1TLE.line1, day1TLE.line2, self.name, self.ts)
        geocentric = self.eSatellite.at(self.t)
        subpoint = geocentric.subpoint()
        self.latitude = subpoint.latitude.radians
        self.longitude = subpoint.longitude.radians

        self.position = geocentric.position.km
        self.velocity = geocentric.velocity.km_per_s
        self.altitude = np.sqrt(self.position[0]**2+self.position[1]**2+self.position[2]**2)       #kilometers from center of Earth
        
        self.UTVisible = False
        self.angleOfVisibility = (62.9 * np.pi/180) # cone radius in radians
        self.prevSeparation = 0
        self.signalFrequency = 1629000 # kHz
        self.c = 299792458/1000*60 # speed of light in kilometers/minute
    
    def advanceTime(self):
        self.date += timedelta(self.timeStep)
        self.t = self.ts.utc(self.date.year, self.date.month, self.date.day, self.date.hour, self.date.minute, self.date.second)
        monthIndex = self.date.month - 1
        dayIndex = self.date.day - 1
        TLENotFound = True
        try:
            self.currentTLE = self.TLEContainer.TLEs[monthIndex][dayIndex][0]
            TLENotFound = False
        except:
            if dayIndex < 7:
                lowerDayIndex = 0
            else:
                lowerDayIndex = dayIndex - 7
            if dayIndex > 24:
                upperDayIndex = 31
            else:
                upperDayIndex = dayIndex + 7
            subsetTLEs = self.TLEContainer.TLEs[monthIndex][lowerDayIndex:upperDayIndex]
            for dayTLEs in subsetTLEs:
                if len(dayTLEs) > 0:
                    self.currentTLE = dayTLEs[0]
                    TLENotFound = False
                    break
        self.eSatellite = EarthSatellite(self.currentTLE.line1,self.currentTLE.line2,self.name,self.ts)
        geocentric = self.eSatellite.at(self.t)
        subpoint = geocentric.subpoint()
        self.latitude = subpoint.latitude.radians
        self.longitude = subpoint.longitude.radians
        self.position = geocentric.position.km
        self.velocity = geocentric.velocity.km_per_s
        self.altitude = np.sqrt(self.position[0]**2+self.position[1]**2+self.position[2]**2)       #kilometers from center of Earth
    
    def angularSeparationSky(self, l0, b0, l, b):
        ''' NOTE: Still working on my geometrical understanding of this. The formula Max
            mentioned was an approximation to the following formula. '''
        cos_d = np.cos(np.pi/2 - b0) * np.cos(np.pi/2 - b) + np.sin(np.pi/2 - b0) * np.sin(np.pi/2 - b) * np.cos(l0 - l)
        d = np.arccos(cos_d) # invert to get the distance
        return d

    def linearSeparation(self, UTAltitude, angularSeparationSky):
        '''
        Use law of cosines to determine the distance between the UT and the 
        satellite node.
        '''
#        UTAltitude += 6371 # add the average radius of the earth in km
        nodeAltitude = self.altitude
        relativeDSquared = (UTAltitude**2 + nodeAltitude**2 - 2*UTAltitude*nodeAltitude*np.cos(angularSeparationSky))
        return np.sqrt(relativeDSquared)

    def angularSeparationNadir(self, angularSeparationSky, linearSeparation, UTAltitude):
        '''
        Use law of sines to determine the angle to the UT from the nadir of the
        satellite node.
        '''
        sin_separation = np.sin(angularSeparationSky)/linearSeparation*(UTAltitude)
        return np.arcsin(sin_separation)
    
    def relativeVelocity(self, newSeparation):
        '''
        Determine the relative velocity between this satellite and the UT
        '''
        distanceCovered = newSeparation - self.prevSeparation
        velocity = distanceCovered/(self.timeStep*60*24) # kilometers/minute
        self.prevSeparation = newSeparation
        return velocity
    
    def dopplerShift(self, relVelocity):
        '''
        Calculate Doppler shift for signals between UT and Satellite. The signals
        are assumed to have a frequency of around 1626 MHz
        '''
        deltaF = (relVelocity/self.c)*self.signalFrequency
        deltaF = np.absolute(deltaF)
        return deltaF
    
    def relativisticDopplerShift(self,UTVelocity,UTPosition):
        c = 299792.458 # km/s
        positionVector = np.asarray(self.position) - np.asarray(UTPosition) # from sender to receiver
        magnitudePosition = np.linalg.norm(positionVector) # magnitude of position vector
        unitVector = positionVector/magnitudePosition # unit position vector
        UTVelocityVector = np.asarray(UTVelocity)
        satVelocityVector = np.asarray(self.velocity)
        
        UTVelocityMagnitude = np.linalg.norm(UTVelocityVector)
        satVelocityMagnitude = np.linalg.norm(satVelocityVector)
        UTVelocityDotUnit = np.dot(unitVector, UTVelocityVector)
        satVelocityDotUnit = np.dot(unitVector, satVelocityVector)
        
        factor1 = (c-UTVelocityDotUnit)/(c-satVelocityDotUnit)
        factor2 = np.sqrt((c**2-UTVelocityMagnitude**2)/(c**2-satVelocityMagnitude**2))
        deltaF = abs(factor1*factor2*self.signalFrequency - self.signalFrequency)
        return deltaF
    
    def isAroundPole(self):
        if (self.plane % 2) == 0:
            if self.latitude < -70*np.pi/180:
                return True
            else:
                return False
        else:
            if self.latitude > 70*np.pi/180:
                return True
            else:
                return False

    def checkUTVisible(self, l, b, UTAltitude, UTPosition, UTVelocity):
        '''
        takes l and b of UT and sets a bool indicating whether the UT
        is within the cone of visibility of this satellite.
        '''
        angularSeparationSky = self.angularSeparationSky(self.longitude, self.latitude, l, b)
        linearSeparation = self.linearSeparation(UTAltitude, angularSeparationSky)
        angularSeparationNadir = self.angularSeparationNadir(angularSeparationSky, linearSeparation, UTAltitude)
        relVelocity = self.relativeVelocity(linearSeparation)
        relativisticDopplerShift = self.relativisticDopplerShift(UTVelocity,UTPosition)
        visible = angularSeparationNadir < self.angleOfVisibility and relativisticDopplerShift < 37.5 and not self.isAroundPole()
        if angularSeparationSky > 21.1*np.pi/180:
            visible = False
        self.UTVisible = visible

class network:
    def __init__(self):
        '''
        network() takes one User Terminal satellite (e.g. cubeSAT) object.
        '''
        self.delta = 5 # seconds
        cubeSATTLEs = "./haloSat.tles"
        self.cubeSAT = cubeSAT(cubeSATTLEs, self.delta)
        
        satNum = 0
        self.satellites = []
        directory = "./iridiumTLEs"
        for filename in os.listdir(directory):
            satNum += 1
            self.satellites.append(satellite(satNum, filename, directory+"/"+filename, self.delta))

    def advanceTime(self):
#         startTime = time.perf_counter()
        self.cubeSAT.advanceTime()

        # non-parallel loop
        for sat in self.satellites:
            sat.advanceTime()
        # parallel loop (4 cores on iMac)
        # pool = mp.Pool(4)
#         pool.map(advanceTimeWorker, (sat for sat in self.satellites))
#         pool.close()
#         pool.join()
#         with mp.Pool(processes=4) as pool:
#             self.satellites = pool.map(self.object.advanceTime, self.satellites)

        l, b = self.cubeSAT.longitude, self.cubeSAT.latitude
        UTAltitude = self.cubeSAT.altitude
        UTVelocity = self.cubeSAT.velocity
        UTPosition = self.cubeSAT.position
        for satellite in self.satellites:
            satellite.checkUTVisible(l,b,UTAltitude,UTPosition,UTVelocity)
#         endTime = time.perf_counter()
    
    def visibilitySimulation(self):
        secondsInYear = 365*24*60*60
        with open("visibilityOutput_haloSat_iridium.txt" , "w") as file:
            startTime = time.perf_counter()
            for i in range(int(secondsInYear/self.delta)):
                if self.cubeSAT.date.year == 2019:
                    for sat in self.satellites:
                        if sat.UTVisible:
                            file.write(sat.name+"\t")
                    file.write("\n")
                    if i%2000 == 0:
                        print("Date in Simulation",self.cubeSAT.date)
                        fractionCompleted = i/(secondsInYear/self.delta)
                        percentCompleted = fractionCompleted * 100
                        elapsedTime = time.perf_counter() - startTime
                        print("elapsed Time", elapsedTime)
                        try:
                            timeRemaining = (elapsedTime/fractionCompleted-elapsedTime)/60/60 # hours
                        except:
                            timeRemaining = "Just Started"
                        print("Percent Completed:", percentCompleted)
                        print("Approximate time remaining (hours):", timeRemaining)
                        print("---------------------------")
                    self.advanceTime()
                    
    def plotCurrentConfiguration(self):
        fig = plt.figure()
        ax = fig.add_subplot(111,projection="aitoff")
        for sat in self.satellites:
            ax.scatter(sat.longitude, sat.latitude)
        plt.show()

class cubeSAT:
    def __init__(self, TLEs, delta):
        self.TLEContainer = TLEsOverYear(TLEs, False)
        breakOuter = False
        # Grab TLE from day 1 or the first one of the year
        try:
            day1TLE = self.TLEContainer.TLEs[0][0][0]
        except:
            for month in range(12):
                if breakOuter:
                    break
                for day in range(31):
                    dayTLEArray = self.TLEContainer.TLEs[month][day]
                    if len(dayTLEArray) > 0:
                        breakOuter = True
                        day1TLE = dayTLEArray[0]
                        break
        
        self.currentTLE = day1TLE
        self.ts = load.timescale(builtin=True)
        self.date = datetime(day1TLE.year, 1, 1) # year, month, day
        self.t = self.ts.utc(day1TLE.year, 1, 1)
        self.timeStep = 1.0/(60*60*24)*delta  # second * delta expressed as fraction of day

        self.name = "haloSAT"
        self.eSatellite = EarthSatellite(day1TLE.line1, day1TLE.line2, self.name, self.ts)
        geocentric = self.eSatellite.at(self.t)
        subpoint = geocentric.subpoint()
        self.latitude = subpoint.latitude.radians
        self.longitude = subpoint.longitude.radians

        self.position = geocentric.position.km
        self.velocity = geocentric.velocity.km_per_s
        self.altitude = np.sqrt(self.position[0]**2+self.position[1]**2+self.position[2]**2)       #kilometers from center of Earth

    def advanceTime(self):
        self.date += timedelta(self.timeStep)
        self.t = self.ts.utc(self.date.year, self.date.month, self.date.day, self.date.hour, self.date.minute, self.date.second)
        monthIndex = self.date.month - 1
        dayIndex = self.date.day - 1
        TLENotFound = True
        try:
            self.currentTLE = self.TLEContainer.TLEs[monthIndex][dayIndex][0]
            TLENotFound = False
        except:
            if dayIndex < 7:
                lowerDayIndex = 0
            else:
                lowerDayIndex = dayIndex - 7
            if dayIndex > 24:
                upperDayIndex = 31
            else:
                upperDayIndex = dayIndex + 7
            subsetTLEs = self.TLEContainer.TLEs[monthIndex][lowerDayIndex:upperDayIndex]
            for dayTLEs in subsetTLEs:
                if len(dayTLEs) > 0:
                    self.currentTLE = dayTLEs[0]
                    TLENotFound = False
                    break
        if TLENotFound:
            print("TLE not found. Using the previous one.")
        self.eSatellite = EarthSatellite(self.currentTLE.line1,self.currentTLE.line2,self.name,self.ts)
        geocentric = self.eSatellite.at(self.t)
        subpoint = geocentric.subpoint()
        self.latitude = subpoint.latitude.radians
        self.longitude = subpoint.longitude.radians
        self.position = geocentric.position.km
        self.velocity = geocentric.velocity.km_per_s
        self.altitude = np.sqrt(self.position[0]**2+self.position[1]**2+self.position[2]**2)       #kilometers from center of Earth
    


if __name__ == '__main__':
    iridiumAMuLET_network = network()
    iridiumAMuLET_network.visibilitySimulation()

import numpy as np
import matplotlib.pyplot as plt

class visibilityMonteCarlo:

    def __init__(self):
        self.satNums = [140,142,143,144,145,146,148,149,150,153,
                        157,113,116,120,130,131,134,135,137,138,
                        141,151,117,118,121,123,126,167,168,171,
                        172,173,180,100,107,119,122,125,128,129,
                        132,133,136,139,108,154,155,156,158,159,
                        160,163,164,165,166,102,103,104,106,109,
                        110,111,112,114,147,152]
        self.visArray = {}
        self.times = []
        self.iterations = 10000
        self.probabilities = []

    def readVisibilityFile(self):
        timeSteps = []
        with open("visibilityOutput_haloSat_iridium.txt", "r") as file:
            for line in file:
                row = line.rstrip()
                row = row.split("\t")
                timeSteps.append(row)
        print("file read")
        return timeSteps

    def createVisibilityArray(self):
        timeSteps = self.readVisibilityFile()
        visibilityArray = {}
        for num in self.satNums:
            visibilityArray[num] = []
        for t in timeSteps:
            if t[0] == "":
                for num in self.satNums:
                    visibilityArray[num].append(0)
            else:
                activeSats = []
                for entry in t:
                    satNum = int(entry[9:-5])
                    activeSats.append(satNum)
                    visibilityArray[satNum].append(1)
                for num in self.satNums:
                    if num not in activeSats:
                        visibilityArray[num].append(0)
        print("visibility Array created")
        self.visArray = visibilityArray

    def satsContacted(self):
        #print("total time steps:", totalTimeSteps)
        for n in self.visArray:
            totalTimeSteps = len(self.visArray[n])
            break
        rng = np.random.rand()
        randomIndex = int(totalTimeSteps*rng)
        #print("Random Index:", randomIndex)
        satsContacted = {}
        for n in self.satNums:
            timeSeries = self.visArray[n][randomIndex:]
            counter = 0
            steps = 0
            for v in timeSeries:
                if v == 0:
                    counter = 0
                    steps += 1
                    continue
                else:
                    counter += 1
                    steps += 1
                if counter > 3:
                    satsContacted[n] = steps
                    break
        return satsContacted

    def checkFastestContact(self):
        satsContacted = self.satsContacted()
        times2Contact = []
        for n in satsContacted:
            time = satsContacted[n]
            times2Contact.append(time)
        times2Contact = np.asarray(times2Contact)
        try:
            shortestTime = times2Contact.min()
        except:
            shortestTime = 60*10000
#         print("Shortest contact time:", shortestTime)
        return shortestTime

    def getTimes(self):
        self.times = []
        for i in range(self.iterations):
            time = self.checkFastestContact()
            print("fastest contact for random sample:", time)
            self.times.append(time)

    def plotProbabilityVsTime(self):
        for i in range(1,6001):
            counter = 0
            for t in self.times:
                if t*5.0 < i:
                    counter += 1
            probability = counter/float(self.iterations)
            self.probabilities.append(probability)
            print("probability of contact within",i,"minutes:", probability)
        plt.plot(range(1,6001), self.probabilities)
        plt.show()
        ts = np.asarray(self.times*5)
        plt.hist(ts, bins=100)
        plt.show()

    def writeOutput(self):
        with open('haloSat-iridium_Probabilities.txt', 'w') as file:
            counter = 0
            for p in self.probabilities:
                counter += 1
                file.write(str(counter))
                file.write('\t')
                file.write(str(p))
                file.write('\n')
        with open('haloSat-iridium_times2FirstContact.txt', 'w') as file:
            for t in self.times:
                actualTime = t*5.0
                file.write(str(actualTime))
                file.write('\n')

if __name__ == '__main__':
    MC = visibilityMonteCarlo()
    MC.createVisibilityArray()
    MC.getTimes()
#     MC.plotProbabilityVsTime()
    MC.writeOutput()

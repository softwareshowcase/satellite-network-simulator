# Satellite Network Visibility Simulator


## Idea

Satellite internet networks have the potential to serve satellite terminals in low-Earth orbit (LEO). Although the networks are meant to serve ground terminals at most latitudes on Earth's surface, a satellite below the altitude of a network (e.g. the Iridium network is at ~780 km) will occasionally come into range of its radio transmitter. This may be an effective complement for the oft used ground stations that, although reliable, are not frequently in contact with LEO satellites. When a project requires short messages to be transmitted at anytime, it is not sufficient to rely on these ground stations.
The tools provided here are meant to help determine whether using such a network for your application is feasible by simulating a year of satellite network configurations along with the position of a user terminal.

## Running the simulation

Clone the repository and run the main program in satTools_TLE.py. It will run the simulation for the Iridium network with the user terminal having the same orbital positions as Halo Sat (a CubeSat). The repository contains a year's worth of TLEs for the Iridium network and a few CubeSats. If you make an account on space-track.org, you can obtain TLEs for just about anything orbiting Earth. You can change the time increment of the simulation to adjust the resolution of the simulation it also scales the actual time the simulation will take on your machine.

## Analyzing the results

Also included in the repository is a Monte Carlo simulation that generates times to first contact for any number of iterations through the output of the visibility simulation. This can be used to calculate the probability of contact time happening within some timeframe.
